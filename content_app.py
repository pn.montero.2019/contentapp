#!/usr/bin/env python3

import socket

PORT = 1234

class webApp:
    """
    Esta clase sirve contenido almacenado en un diccionario Python
    """

    def __init__(self, hostname, port, content_dict):
        """
        Inicializamos el servidor web con el diccionario
        """
        self.content_dict = content_dict    # Guardamos el diccionario
        self.hostname = hostname    # Guardamos el nombre del host
        self.port = port    # Guardamos el puerto
        self.inicialize_server()    # Inicializamos el servidor

    def inicialize_server(self):

        # Creamos un socket
        self.my_Server_Socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        # Configuramos el socket para reutilizar la dirección
        self.my_Server_Socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        # Enlazamos el socket al host y puerto especificados
        self.my_Server_Socket.bind((self.hostname, self.port))

        # Ponemos el socket en modo escucha con una cola de hasta 5 conexiones
        self.my_Server_Socket.listen(5)

    def parse (self, request):
        """ Método Parse recibe la solicitud del cliente, la analiza
         y extrae la información relevante """

        # Dividimos el recurso en líneas
        lineas = request.split("\n")

        # Tomamos la primera linea que contendrá a la solicitud GET
        request_linea = lineas[0]

        # Dividimos la linea en palabras
        words = request_linea.split()

        # Comprobamos si hay al menos dos palabras (GET y la ruta del recurso)
        if len(words) >= 2:
            return words[1]     # devuelve la ruta del recurso

        return None     # de lo contarrio devolvemos None

    def serve_content(self, resource):
        """ Este método sirve el contenido asociado al recurso solicitado """
        if resource in self.content_dict:
            return "200 OK", self.content_dict[resource]
        else:
            HTML = "<html><body><h1>404 Not Found</h1></body></html>"
            return "404 Noy Found", HTML

    def run(self):
        """ Inicia el servidor y comienza a escuchar las solicitudes entrantes. """

        # Mostramos en pantalla que el servidor está en ejecución
        print(f"Servidor en ejecución en {self.hostname} y {self.port}")

        while True: # bucle infinito para esperar conexiones
            print("Waiting for connections")
            # recvSocket es el client_socket y addres es la tupla (ip, port) del cliente
            (recvSocket, address) = self.my_Server_Socket.accept()  # Aceptamos una conexión entrante
            print("HTTP request received from:", address)
            request = recvSocket.recv(2048) # recibimos la solicitud del cliente
            print("Request received: ")
            print(request)
            resource = self.parse(request.decode('utf8'))

            # Si se ha encontrado un recurso válido en la solicitud
            if resource:
                print("Requesting resource: ", resource)

                # Servimos el contenido del recurso solicitado
                (returnCode, htmlAnswer) = self.serve_content(resource)

                print("Answering back...")
                # Creamos la respuesta HTTP
                response = "HTTP/1.1 " + returnCode + " \r\n\r\n" + htmlAnswer + "\r\n"
            else:   # si la solicitud no es válida
                response = "HTTP/1.1 400 Bad Request\r\n\r\nInvalid request"

            recvSocket.send(response.encode('utf8'))    # Codificamos y enviamos la respuesta al cliente
            recvSocket.close()  # Cerramos la conexión con el cliente


if __name__ == "__main__":
    # Contenido del servidor: Recurso como clave y contenido HTML como valor
    content_dict = {
        "/": "<html><body><h1>Pagina de inicio</h1><p>Bienvenido a esta prueba!</p></body></html>",
        "/about": "<html><body><h1>Esta es una appWeb simple</h1><p>Sirve el contenido de recursos almacenados en un "
                  "diccionario</p></body></html>",
        "/hola": "<html><body><h2>Hola mundo maravilloso!<h2><body><html>",
        "/adios": "<html><body><h2>Adios mundo cruel!<h2><body><html>",
    }

    # Creamos una instancia de WebApp y ejecutamos el servidor en el puerto 1234
    testWebApp = webApp("localhost", PORT, content_dict)
    testWebApp.run()